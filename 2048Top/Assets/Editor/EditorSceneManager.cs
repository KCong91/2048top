﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public class EditorSceneManager
{
    static private void LoadScene(string inputScene)
    {
        EditorApplication.SaveCurrentSceneIfUserWantsTo();
        EditorApplication.OpenScene(inputScene);
    }

    [MenuItem ("Scene/Play")]
    static public void PlayGame()
    {
        if(true == EditorApplication.isPlaying)
        {
            EditorApplication.isPlaying = false;
            return;
        }
        EditorApplication.SaveCurrentSceneIfUserWantsTo();
        EditorApplication.OpenScene("Assets/Scenes/LoadingScene.unity");
        EditorApplication.isPlaying = true;
    }

    [MenuItem ("Scene/LoadingScene")]
    static public void OpenLoadingScene()
    {
        LoadScene("Assets/Scenes/LoadingScene.unity");
    }
    
    [MenuItem("Scene/LogInScene")]
    static public void OpenLogInScene()
    { 
        LoadScene("Assets/Scenes/LogInScene.unity");
    }

    [MenuItem("Scene/LobbyScene")]
    static public void OpenLobbyScene()
    {
        LoadScene("Assets/Scenes/LobbyScene.unity");
    }
}
