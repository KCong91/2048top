﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDLogInSceneManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnLogInSuccess()
    {
        StartCoroutine(SceneManager.Instance.LoadScene(SceneManager.SCENE_LOBBY, 1.5f));
    }

    public void OnBtnFacebook_Pressed()
    {
        OnLogInSuccess();
    }

    public void OnBtnTwitter_Pressed()
    {
        OnLogInSuccess();
    }

    public void OnBtnGmail_Pressed()
    {
        OnLogInSuccess();
    }

    public void OnBtnGuest_Pressed()
    {
        //send login infor to server
        if (true == VariablesGlobal.g_GameServerCore.InitHost(Define.HOST, Define._2048TOP_PORT, MyNum.USER_TYPE.UT_GUEST, ""))
        {
            //OnLogInSuccess();
        }
    }

}
