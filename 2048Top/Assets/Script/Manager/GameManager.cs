﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    private void Start()
    {
        Debug.Log("======> GameManager Start ");

    }

    private GameManager()
    {
    }

    public static GameManager Instance
    {
        get
        {
            if (null == instance)
            {
                GameObject gameManager = new GameObject();
                instance = gameManager.AddComponent<GameManager>();
                DontDestroyOnLoad(instance.gameObject);
            }
            return instance;
        }
    }

    public void Initialize()
    {

    }

    void OnDestroy()
    {
        Debug.Log("======> OnDestroy ");
        VariablesGlobal.g_GameServerCore.Uninit();
    }

}
