﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingSceneManager : MonoBehaviour
{
    private void Awake()
    {
        GameManager.Instance.Initialize();
        SceneManager.Instance.Initialize();
    }
    // Start is called before the first frame update
    void Start()
    {
        SceneManager.Instance.LoadSingleScene(SceneManager.SCENE_LOGIN);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
