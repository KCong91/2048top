﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManager : MonoBehaviour
{
    private static SceneManager instance;

    public const string SCENE_LOGIN = "LogInScene";
    public const string SCENE_LOBBY = "LobbyScene";

    private void Start()
    {
        Debug.Log("======>SceneManager Start ");

    }

    private SceneManager()
    {
    }

    public static SceneManager Instance
    {
        get
        {
            if (null == instance)
            {
                GameObject sceneManager = new GameObject();
                instance = sceneManager.AddComponent<SceneManager>();
                DontDestroyOnLoad(instance.gameObject);
            }
            return instance;
        }
    }

    public void Initialize()
    {

    }

    public void LoadSingleScene(string sceneName)
    {
        Debug.Log("Load new scene " + sceneName);

        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

    public IEnumerator LoadScene(string sceneName, float delayTime)
    {
        Debug.Log("Load new scene " + sceneName);
        yield return new WaitForSeconds(delayTime);
        
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

}
