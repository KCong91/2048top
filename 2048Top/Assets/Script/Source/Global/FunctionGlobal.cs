﻿using System.Collections;
using System.Runtime.InteropServices;
using System;
using System.Text;

public class FunctionGlobal {

    public static byte[] StructtoArrByte(Object obj)
    {
        int Size = Marshal.SizeOf(obj);
        byte[] pData = new byte[Size];

        IntPtr ptr = Marshal.AllocHGlobal(Size);

        Marshal.StructureToPtr(obj, ptr, true);
        Marshal.Copy(ptr, pData, 0, Size);
        Marshal.FreeHGlobal(ptr);
        return pData;
    }

    public static Object ArrByteToStructByOffset(byte[] pData, int nOffset, Object obj)
    {
        int len = Marshal.SizeOf(obj);

        IntPtr i = Marshal.AllocHGlobal(len);

        Marshal.Copy(pData, nOffset, i, len);

        Object obj_ = Marshal.PtrToStructure(i, obj.GetType());

        Marshal.FreeHGlobal(i);

        return obj_;
    }

    public static byte[] GetBytes(byte[] bytes, int nOffset, int nSize)
    {
		int nLen = nSize;
		byte[] pData = null;
		if (nLen < 1)
			nLen = bytes.Length - nOffset;
		pData = new byte[nLen];

		Buffer.BlockCopy (bytes, nOffset, pData, 0, nLen);

		return pData;
	}

	public static void SetByteValue(ref byte[] pData, byte value) {
		for (int i = 0; i < pData.Length; i++)
		{
			pData[i] = value;
		}
	}

	public static void CopyByte(string s, ref byte[] bytes) {
		SetByteValue(ref bytes, 0);
		System.Buffer.BlockCopy(s.ToCharArray(), 0, bytes, 0, s.Length * sizeof(char));
	}

    public static void CopyBytes(byte[] dataSource, int nIndexSource, byte[] dataDest, int nIndexDest, int nCount)
    {
        System.Buffer.BlockCopy(dataSource, nIndexSource, dataDest, nIndexSource, nCount);
    }

    public static void CopyBytes(byte[] dataSource, byte[] dataDest)
    {
        System.Buffer.BlockCopy(dataSource, 0, dataDest, 0, dataSource.Length);
    }

    public static void CopyBytes(byte[] dataSource, byte[] dataDest, int nIndexSource)
    {
        System.Buffer.BlockCopy(dataSource, 0, dataDest, nIndexSource, dataSource.Length);
    }

    /*public uint calc_checksum( uint crc_accum, byte[] data_blk_ptr, uint data_blk_size)
	{
		uint i, j;
		
		for(j=0; j<data_blk_size; j++)
		{
			i = ((uint)(crc_accum >> 24) ^ *data_blk_ptr++) & 0xff;
			crc_accum = (crc_accum << 8) ^ crc_table[i];
		}
		return crc_accum;
	}*/

    public static uint calc_MakeCheckHack(uint CheckHack) {
		Random rnd = new Random ();

		byte[] pTmp = BitConverter.GetBytes (CheckHack);
		pTmp[2] = (byte)rnd.Next(0, Define.UCHAR_MAX);
		pTmp[3] = (byte)rnd.Next(0, Define.UCHAR_MAX);

		if (pTmp [3] % 2 == 0) {
			byte tmp = pTmp[1];
			pTmp[1] = pTmp[2];
			pTmp[2] = tmp;
		}

		return BitConverter.ToUInt32 (pTmp, 0);
	}

    //<=== cần sửa lai hàm này
	public static string BytsString2String(byte[] b) {
        /*int i = 0;
		while (i < b.Length) {
			if (b[i] == 0 && b[i+1] == 0)
				break;
			i += 2;
		}

        byte[] p = new byte[i];

		System.Buffer.BlockCopy (b, 0, p, 0, p.Length);

        return Encoding.Unicode.GetString(p);*/
        //Encoding ecd = Encoding.GetEncoding("utf-16");
        Encoding ecd = Encoding.GetEncoding("utf-8");
        return ecd.GetString(b, 0, b.Length);
    }

}
