﻿using UnityEngine;
using System.Collections;

public class GameServerCore
{
    public UDPManager m_UDPM = null;
    public TcpSocket m_TcpSocket = null;

    string GetIPFromHost(string sHost)
    {
        string sIP = "";

        System.Net.IPHostEntry ih = System.Net.Dns.GetHostEntry(sHost);
        System.Net.IPAddress[] ip = ih.AddressList;

        sIP = ip[ip.Length - 1].ToString();
        return sIP;
    }
    public bool InitHost(string sHost, ushort uPort, MyNum.USER_TYPE ut, string sUserID)
    {
        string sIP = GetIPFromHost(sHost);
        return InitIP(sIP, uPort, ut, sUserID);
    }

    public bool InitIP(string sIP, ushort uPort, MyNum.USER_TYPE ut, string sUserID)
    {
        Uninit();

        sIP = "192.168.1.13";
        //sIP = "169.254.169.254";
        //sIP = "35.236.128.192";
        //uPort = 8080;

        m_TcpSocket = new TcpSocket();
        if (!m_TcpSocket.Connect(sIP, uPort))
            return false;
        
        VariablesGlobal.g_ThreadManager.Init(m_TcpSocket);

        MakeLogin(ut, sUserID);

        return true;
    }

    public void Uninit()
    {
        if (m_UDPM != null)
        {
            //MakeLogout(m_MyChar.GetGUID());
            //m_ThreadMamanger.Uninit();

            m_UDPM.Uninit();
            m_UDPM = null;

            //m_MyChar.ClearAllData();
            //m_MyChar = null;
        }

        //m_WorldManager.Uninit();

        if (m_TcpSocket != null)
        {
            m_TcpSocket.Close();
            m_TcpSocket = null;
        }
            
    }

    void MakeLogin(MyNum.USER_TYPE ut, string sUserID)
    {
        NetData nd = new NetData((ushort)MyNum.MAIN_ACTION.LOGIN);
        nd.WriteUint(Define.__Version);
        nd.WriteByte((byte)ut);
        nd.WriteString(sUserID);
        nd.WriteString(""); //pass
        nd.WriteString("DeviceID");
        nd.WriteString("MacAddr");
        nd.WriteString("IPAddr");
        VariablesGlobal.g_ThreadManager.AddDataSend(nd);
    }

} //GameServerCore
