﻿using System.Collections;

public class Define {

	public const uint __Version = 2019101601;

	//My Define
	public const int MAX_MESSAGE_SIZE = 128;
	//My Define

    public const int MAX_PACKAGE_SIZE = 4096;
    public const int MAX_FAMNAME_STRING = 64;
    
	public const int REQUIRED_NUM = 3;

	public const int UCHAR_MAX = 0xff;

	public const int USHORT_MAX = 0xffff;

	public const int MAX_NUM_CHECK_HACK = USHORT_MAX - 5;
    	
	public const int MAX_ENAME_STRING = 20;

	public const int LEN_IP_MAC = 32;

    public const uint UINT_MAX = 0xfffffffa;

    public const string HOST = "35.240.172.111";

    public const ushort _2048TOP_PORT = 2019;
}
