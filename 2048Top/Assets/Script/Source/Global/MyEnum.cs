﻿public class MyNum
{
    public enum MAIN_ACTION
    {
        NONE =0,
        LOGIN,
    }

    public enum SUB_ACTION
    {
        NONE = 0,
        LOGIN,
    }

    public enum USER_TYPE
    {
        UT_FACEBOOK = 1,
        UT_TWITTER,
        UT_GMAIL,
        UT_GUEST,
    }

    public enum RESULT { FAIL=0, SUCESS=1, };

    public enum MEMBER_UPDATE_TYPE { ADD=1, REMOVE=2};

    public enum SEX { MALE}

    public enum PACKAGE_TYPE { NORMAL = 0, ACK }

    public enum GAME_STATUS { }
}