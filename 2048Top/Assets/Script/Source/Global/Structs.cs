﻿using System.Collections;
using System.Collections.Generic;

public struct HEADER
{
    ushort uMain;
    ushort uSub;
}

public struct DATA
{
    ushort uLen;
    byte[] pData;
}


