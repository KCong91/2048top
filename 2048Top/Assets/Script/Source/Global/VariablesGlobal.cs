﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariablesGlobal
{
    public static MyLog g_Log = new MyLog();
    public static GameServerCore g_GameServerCore = new GameServerCore();
    public static ThreadManager g_ThreadManager = new ThreadManager();

}
