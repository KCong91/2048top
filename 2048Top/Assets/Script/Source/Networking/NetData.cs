﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.InteropServices;

/*
 * Header:
 *      Main:       ushort    //Main Action
 *      Sub:        ushort    //Suba ction
 *      DataLen:    ushort    //len of data above
 * Data:
 *      data:       byte[]
 */
public class NetData
{
    byte[] m_pData = new byte[Define.MAX_PACKAGE_SIZE];
    int m_nReadPos = 0;
    int m_nWritePos = 0;

    public const int MAIN_ACTION_POS = 0;
    public const int SUB_ACTION_POS = MAIN_ACTION_POS + sizeof(ushort);
    public const int DATA_LEN_POS = SUB_ACTION_POS + sizeof(ushort);
    public const int DATA_POS = DATA_LEN_POS + sizeof(ushort);
    
    public NetData()
    {
    }

    public NetData(ushort uMainAction, ushort uSubAction, byte[] pData)
    {
        WriteUshort(uMainAction);
        WriteUshort(uSubAction);
        WriteUshort((ushort)pData.Length);
        Write(pData);
    }

    public NetData(ushort uMainAction, ushort uSubMain)
    {
        WriteUshort(uMainAction);
        WriteUshort(uSubMain);
    }

    public NetData(ushort uMainAction)
    {
        WriteUshort(uMainAction);
        WriteUshort(0);
    }

    public NetData(HEADER header)
    {
        Write(header);
    }

    public int GetSize()
    {
        return m_nWritePos;
    }

    public void FromData(byte[] pData)
    {
        FunctionGlobal.CopyBytes(pData, m_pData);
        SetWritePos(pData.Length);
    }

    public byte[] ToBytes()
    {
        return FunctionGlobal.GetBytes(m_pData, 0, m_nWritePos);
    }

    public ushort GetMainAction()
    {
        int nOldReadPos = GetReadPos();
        SetReadPos(MAIN_ACTION_POS);
        ushort uMain = ReadUshort();
        SetReadPos(nOldReadPos);
        return uMain;
    }

    public ushort GetSubAction()
    {
        int nOldReadPos = GetReadPos();
        SetReadPos(SUB_ACTION_POS);
        ushort uSub = ReadUshort();
        SetReadPos(nOldReadPos);
        return uSub;
    }

    public ushort GetDataLength()
    {
        int nOldReadPos = GetReadPos();
        SetReadPos(DATA_LEN_POS);
        ushort uSub = ReadUshort();
        SetReadPos(nOldReadPos);
        return uSub;
    }

    public byte[] Read(int nSize)
    {
        byte[] pData = FunctionGlobal.GetBytes(m_pData, m_nReadPos, nSize);
        m_nReadPos += nSize;
        return pData;
    }

    public void Write(Object o)
    {
        byte[] pData = FunctionGlobal.StructtoArrByte(o);
        Write(pData);
    }

    public bool Write(byte[] pData)
    {
        if (pData.Length > m_pData.Length - m_nWritePos)
            return false;
        else
            Buffer.BlockCopy(pData, 0, m_pData, m_nWritePos, pData.Length);

        m_nWritePos += pData.Length;
        return true;
    }

    public byte ReadByte()
    {
        byte[] pData = Read(sizeof(byte));
        if (pData == null)
            return 0;
        return pData[0];
    }

    public bool WriteByte(byte bValue)
    {
        byte[] pData = new byte[1];
        pData[0] = bValue;
        return Write(pData);
    }

    public uint ReadUint()
    {
        byte[] pData = Read(sizeof(uint));
        if (pData == null)
            return 0;
        return BitConverter.ToUInt32(pData, 0);
    }

    public bool WriteUint(uint uValue)
    {
        byte[] pData = BitConverter.GetBytes(uValue);
        return Write(pData);
    }

    public float ReadFloat()
    {
        byte[] pData = Read(sizeof(float));
        if (pData == null)
            return 0;
        return BitConverter.ToSingle(pData, 0);
    }

    public bool WriteFloat(float fValue)
    {
        byte[] pData = BitConverter.GetBytes(fValue);
        return Write(pData);
    }

    public short ReadShort()
    {
        byte[] pData = Read(sizeof(short));
        if (pData == null)
            return 0;
        return (short)BitConverter.ToUInt16(pData, 0);
    }

    public bool WriteShort(short uValue)
    {
        byte[] pData = BitConverter.GetBytes(uValue);
        return Write(pData);
    }

    public ushort ReadUshort()
    {
        byte[] pData = Read(sizeof(ushort));
        if (pData == null)
            return 0;
        return BitConverter.ToUInt16(pData, 0);
    }

    public bool WriteUshort(ushort uValue)
    {
        byte[] pData = BitConverter.GetBytes(uValue);
        return Write(pData);
    }

    public string ReadString()
    {
        short uLen = ReadShort();
        byte[] tmp = Read(uLen);
        string s = FunctionGlobal.BytsString2String(tmp);
        return s;
    }

    public void WriteString(string sValue)
    {
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(sValue);
        short uLen = (short)bytes.Length;
        WriteShort(uLen);
        if (uLen > 0)
            Write(bytes);
    }

    public void SetReadPos(int nReadPos)
    {
        m_nReadPos = nReadPos;
    }

    public int GetReadPos()
    {
        return m_nReadPos;
    }

    public void SetWritePos(int nWritePos)
    {
        m_nWritePos = nWritePos;
    }

    public int GetWritePos()
    {
        return m_nWritePos;
    }

}
