﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;

public class TcpSocket
{
    private TcpClient m_socketConnection = null;
    
    public bool Connect(string sServer, ushort uPot)
    {
        try
        {
            m_socketConnection = new TcpClient(sServer, 2019);
            return true;
        } catch (SocketException ex)
        {
            Debug.Log("Connect fail: " + ex.Message);
            return false;
        }
    }

    public void Close()
    {
        m_socketConnection.Close();
        m_socketConnection = null;
    }

    public bool SendData(NetData nd)
    {
        if (m_socketConnection == null)
            return false;
        try
        {
            NetworkStream stream = m_socketConnection.GetStream();
            byte[] data = nd.ToBytes();
            stream.Write(data, 0, data.Length);
            return true;
        } 
        catch(SocketException socketException)
        {
            Debug.Log("Socket exception: " + socketException);
            return false;
        }
    }

    public NetData ReceiveData()
    {
        return null;
    }


}
