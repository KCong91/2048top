﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System;

public class UDPManager
{
    IPEndPoint m_remoteEndPoint = null;
    UdpClient m_client = null;

    public UDPSend m_UDPSend = null;
    public UDPReceive m_UDPReceive = null;

    public UDPManager()
    {

    }

    public bool Init(string sIP, int uPort)
    {
        try
        {
            m_remoteEndPoint = new IPEndPoint(IPAddress.Parse(sIP), uPort);
            m_client = new UdpClient();
            
            m_UDPSend = new UDPSend(m_remoteEndPoint, m_client);
            if (!m_UDPSend.Init())
                return false;

            m_UDPReceive = new UDPReceive(m_remoteEndPoint, m_client);
            if (!m_UDPReceive.Init())
            {
                m_UDPSend.Uninit();
                m_UDPSend = null;
            }

            return true;

        }
        catch(Exception ex)
        {
            VariablesGlobal.g_Log.Log(ex.Message);
        }
        return false;
    }

    public void Uninit()
    {
        if (m_UDPSend != null)
            m_UDPSend.Uninit();

        if (m_UDPReceive != null)
            m_UDPReceive.Uninit();

        m_client.Close();
        m_client = null;
        m_remoteEndPoint = null;
    }

    public UDPSend GetUDPSend()
    {
        return m_UDPSend;
    }

    public UDPReceive GetUDPReceive()
    {
        return m_UDPReceive;
    }

    public UdpClient GetUdpClient()
    {
        return m_client;
    }

    public byte[] ReceiveData()
    {
        if (m_client == null)
            return null;

        return m_client.Receive(ref m_remoteEndPoint);
    }
    
}
