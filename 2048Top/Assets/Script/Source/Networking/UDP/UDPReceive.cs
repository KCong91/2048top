﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.Net;

public class UDPReceive
{
    UdpClient m_client = null;
    IPEndPoint m_remoteEndPoint = null;

    public UDPReceive(IPEndPoint ep, UdpClient udpClient)
    {
        m_remoteEndPoint = ep;
        m_client = udpClient;
    }

    public bool Init()
    {
        return true;
    }

    public void Uninit()
    {

    }

    public byte[] ReceiveData()
    {
        //IPEndPoint anyIP = new IPEndPoint(IPAddress.Any, 0);
        //byte[] data = m_client.Receive(ref anyIP);
        //anyIP;
        int nSize = m_client.Available;
        if (nSize <= 0)
            return null;

        byte[] bData = new byte[nSize];
        //int nRec = m_client.Client.Receive(bData);
        byte[] aaa = m_client.Receive(ref m_remoteEndPoint);
        //int nRec = m_client.Client.Receive(bData);

        //if (nRec < nSize)
        //    return FunctionGlobal.GetBytes(bData, 0, nRec);

        //return bData;
        return aaa;
    }
    
}
