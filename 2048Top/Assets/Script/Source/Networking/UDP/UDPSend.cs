﻿using System.Collections;
using System.Collections.Generic;

using System.Net;
using System.Net.Sockets;
using UnityEngine;
using System;
using System.Text;

public class UDPSend
{
    private static int localPort;

    // prefs
    //private string IP;  // define in init
    //public int port;  // define in init

    // gui
    //string strMessage = "";

    IPEndPoint m_remoteEndPoint = null;
    UdpClient m_client = null;

    public UDPSend(IPEndPoint ep, UdpClient udp)
    {
        m_remoteEndPoint = ep;
        m_client = udp;
    }

    public bool Init()
    {
        try
        {
            //Debug.Log("UDPSend.init()");

            // define
            //IP = "192.168.10.221";
            //port = 8009;

            // ----------------------------
            // Senden
            // ----------------------------
            //m_remoteEndPoint = new IPEndPoint(IPAddress.Parse(IP), port);
            //m_client = new UdpClient();

            // status
            //Debug.Log("Sending to " + IP + " : " + port);
            //Debug.Log("Testing: nc -lu " + IP + " : " + port);

            //string text = "message: aa";
            /*string text = "123456";

            byte[] data = Encoding.UTF8.GetBytes(text);

            int nn = m_client.Send(data, data.Length, m_remoteEndPoint);*/
        }
        catch (Exception ex)
        {
            Debug.Log("Init.Init ===> " + ex.Message);
            return false;
        }


        return true;
    }

    public void Uninit()
    {
    }

    public bool SendData(string sData)
    {
        byte[] data = Encoding.UTF8.GetBytes(sData);
        int nLen = data.Length;
        return nLen != m_client.Send(data, data.Length, m_remoteEndPoint);
    }

    public bool SendData(byte[] pData)
    {
        int nLen = pData.Length;
        //return nLen != m_client.Send(pData, pData.Length, m_remoteEndPoint);
        int nSend = m_client.Send(pData, pData.Length, m_remoteEndPoint);
        return nLen != nSend;
    }
}
