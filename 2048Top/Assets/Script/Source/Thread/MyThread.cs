﻿using System.Collections;
using System.Threading;
using System;

public class MyThread {
	protected bool isRunning = false;
	
	Thread m_thread = null;
	
	public bool Start() {
		try {
			m_thread = new Thread(new ThreadStart(Working));
			
			isRunning = true;
			m_thread.Start();
			return true;
		} catch (Exception) {
			return false;
		}
	}
	
	public virtual void Stop() {
		try {
			isRunning = false;
			m_thread.Join();
			m_thread = null;
		}
		catch (Exception)
		{ }
	}
	
	public virtual void Working()
	{
	}
}
