﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class ProcessDataThread : MyThread {

	ProcessData m_ProcessData = new ProcessData ();
    
    List<NetData>	m_arrData = new List<NetData>();

	static Semaphore sem = new Semaphore(1, 1);

	public void Add(NetData nd) {
		m_arrData.Add (nd);
	}

	private NetData GetData() {
        while (m_arrData.Count > 1)
        {
            NetData nd = m_arrData[0];
            m_arrData.RemoveAt(0);
            if (nd != null)
                return nd;
        }
		return null;
	}

	public override void Working()
	{
		NetData nd = null;
		
		while (isRunning) {
			if (!isRunning)
				break;

            nd = GetData();
			if (nd == null) {
				Thread.Sleep(50);
				continue;
			}
                        
			Thread.Sleep(5);
		}
	}
}
