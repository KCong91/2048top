﻿using System.Collections;
using System.Threading;

public class ReceiveThread : MyThread {
    private TcpSocket m_TCP = null;
    ThreadManager m_ThreadManager = null;
    
    public ReceiveThread(TcpSocket tcp, ThreadManager tm)
    {
        m_TCP = tcp;
        m_ThreadManager = tm;
	}
	
	private NetData ReceiveData()
    {
        if (m_TCP == null || !isRunning)
            return null;
        return m_TCP.ReceiveData();
	}

    public void SetUDPManager(TcpSocket tcp)
    {
        m_TCP = tcp;
    }
    
	public override void Working()
	{
		NetData nd = null;
		
		while (isRunning)
		{
			if (!isRunning)
				break;

            nd = ReceiveData();
			if (nd == null)
			{
				Thread.Sleep(50);
				continue;
			}

            m_ThreadManager.GetProcessDataThread().Add(nd);
            			
			Thread.Sleep(5);
		}
	}
}
