﻿using System.Collections;
using System.Threading;

public class SendThread : MyThread {
    private TcpSocket m_TCP = null;

    ArrayList m_NetDatas = new ArrayList();

    public SendThread(TcpSocket tcp) {
        m_TCP = tcp;
	}

    public void SetTcpSocket(TcpSocket tcp)
    {
        m_TCP = tcp;
    }
    
    public override void Working()
	{
        NetData nd = null;
        
		while (isRunning)
		{
			if (!isRunning)
				break;

            if (IsEmptyData())
            {
                Thread.Sleep(50);
                continue;
            }
            
            nd = GetData();
            if (nd != null)
            {
                m_TCP.SendData(nd);
                Thread.Sleep(5);
            }
            else
				Thread.Sleep(50);
		}
	}

    bool IsEmptyData()
    {
        return m_NetDatas.Count < 1;
    }

    public void AddData(NetData nd)
    {
        m_NetDatas.Add(nd);
    }

    NetData GetData()
    {
        if (m_NetDatas.Count < 1)
            return null;
        NetData nd = (NetData)m_NetDatas[0];
        m_NetDatas.RemoveAt(0);
        return nd;
    }

    public void MyStop()
    {
        while (!IsEmptyData())
        {
            Thread.Sleep(50);
        }
    }
}
