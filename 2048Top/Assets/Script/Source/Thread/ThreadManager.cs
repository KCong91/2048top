﻿using System.Collections;
using System.Collections.Generic;

public class ThreadManager
{
    SendThread m_SendThread = null;
    ReceiveThread m_ReceiveThread = null;
    ProcessDataThread m_ProcessDataThread = null;

    uint m_uSegNo = 0;

    public void StartThread(TcpSocket tcp)
    {
        StopThread();

        m_ProcessDataThread = new ProcessDataThread();
        m_ProcessDataThread.Start();

        m_SendThread = new SendThread(tcp);
        m_SendThread.Start();

        m_ReceiveThread = new ReceiveThread(tcp, this);
        m_ReceiveThread.Start();
    }

    public void StopThread()
    {
        if (m_SendThread != null)
        {
            m_SendThread.MyStop();
            m_SendThread = null;
        }

        if (m_ReceiveThread != null)
        {
            m_ReceiveThread.Stop();
            m_ReceiveThread = null;
        }

        if (m_ProcessDataThread != null)
        {
            m_ProcessDataThread.Stop();
            m_ProcessDataThread = null;
        }
    }

    public void Init(TcpSocket tcp)
    {
        StartThread(tcp);
    }

    public void Uninit()
    {
        StopThread();
    }

    public void AddDataSend(NetData nd)
    {
        m_SendThread.AddData(nd);
    }

    uint GeneralSegno()
    {
        return m_uSegNo = m_uSegNo++ % Define.UINT_MAX;
    }

    public ProcessDataThread GetProcessDataThread()
    {
        return m_ProcessDataThread;
    }

    public ReceiveThread GetReceiveThread()
    {
        return m_ReceiveThread;
    }

    public SendThread GetSendThread()
    {
        return m_SendThread;
    }

} //ThreadManager
